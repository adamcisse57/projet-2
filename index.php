<?php
session_start(); // Pour utiliser des sessions
require './config.php';

$errorMessage = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = $_POST["username"];
    $password = $_POST["password"];

    $sql = "SELECT * FROM utilisateurs WHERE nom_utilisateur = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("s", $username);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows == 0) { // Utilisateur non trouvé
        $errorMessage = "Utilisateur non trouvé.";
    } else {
        $user = $result->fetch_assoc();
        $hashed_password = $user["mot_de_passe"]; // Récupérer le mot de passe haché

        if (password_verify($password, $hashed_password)) {
            // Stocker des informations dans la session
            $_SESSION["user_id"] = $user["id"];
            $_SESSION["username"] = $username;

            header("Location: Home.php");
            exit;
        } else {
            $errorMessage = "Mot de passe incorrect.";
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Connexion</title>
    <link rel="stylesheet" href="styles/style.css">
</head>
<body>

<div class="container">
    <h2>Connexion</h2>

    <?php if (!empty($errorMessage)): ?>
        <div class="alert alert-warning">
            <strong><?php echo $errorMessage; ?></strong>
        </div>
    <?php endif; ?>

    <form method="post">
        <div class="mb-3">
            <label pour="username">Nom d'utilisateur</label>
            <input type="text" name="username" class="form-control" required>
        </div>
        <div class="mb-3">
            <label pour="password">Mot de passe</label>
            <input type="password" name="password" class="form-control" required>
        </div>
        <button type="submit" class="btn btn-primary">Se connecter</button>
        <a href="enregistrement.php">S'inscrire ?</a>

    </form>
</div>

</body>
</html>
