<?php
session_start();
session_destroy(); // Détruire la session
header("Location: connexion.php"); // Redirection vers la page de connexion
exit;
?>
