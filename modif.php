<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "monsite";

// Créer la connexion à la base de données
$connection = new mysqli($servername, $username, $password, $dbname);

// Vérifier la connexion
if ($connection->connect_error) {
    die("Connexion échouée: " . $connection->connect_error);
}

$id = "";
$nom = "";
$email = "";
$telephone = "";
$adresse = "";

$errorMessage = "";
$successMessage = "";

if ($_SERVER["REQUEST_METHOD"] == "GET") {
    // Vérifier si le paramètre "id" est défini dans la requête GET
    if (isset($_GET["id"])) {
        $id = $_GET["id"];

        // Exécuter la requête SQL pour récupérer les données du client
        $sql = "SELECT * FROM clients WHERE id = $id";
        $result = $connection->query($sql);

        if ($result && $result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $nom = $row["nom"];
            $email = $row["email"];
            $telephone = $row["telephone"];
            $adresse = $row["adresse"];
        } else {
            header("location: /Mon site/index.php");
            exit;
        }
    }
} elseif ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Vérifier si les données POST sont définies
    if (isset($_POST["id"], $_POST["nom"], $_POST["email"], $_POST["telephone"], $_POST["adresse"])) {
        $id = $_POST["id"];
        $nom = $_POST["nom"];
        $email = $_POST["email"];
        $telephone = $_POST["telephone"];
        $adresse = $_POST["adresse"];

        // Exécuter la requête SQL pour mettre à jour les données du client
        $stmt = $connection->prepare("UPDATE clients SET nom = ?, email = ?, telephone = ?, adresse = ? WHERE id = ?");
        $stmt->bind_param("ssssi", $nom, $email, $telephone, $adresse, $id);
        if ($stmt->execute()) {
            $successMessage = "Données mises à jour avec succès.";
        } else {
            $errorMessage = "Erreur lors de la mise à jour des données: " . $stmt->error;
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mon Site</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>

<div class="container my-5">
    <h2>Modifier un client</h2>

    <?php
    if (!empty($errorMessage)) {
        echo "
        <div class='alert alert-warning alert-dismissible fade show' role='alert'>
        <strong>".$errorMessage."</strong>
        <button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button>
        ";
    }
    ?>


    <form method="post">

        <input type="hidden" name="id" value="<?php echo $id; ?>">
        <div class="row mb-3">
            <label class="col-sm-3 col-form-label">Nom</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="nom" value="<?php echo $nom; ?>">
            </div>
        </div>

        <div class="row mb-3">
            <label class="col-sm-3 col-form-label">Email</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="email" value="<?php echo $email; ?>">
            </div>
        </div>

        <div class="row mb-3">
            <label class="col-sm-3 col-form-label">Telephone</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="telephone" value="<?php echo $telephone; ?>">
            </div>
        </div>

        <div class="row mb-3">
            <label class="col-sm-3 col-form-label">Adresse</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="adresse" value="<?php echo $adresse; ?>">
            </div>
        </div>

        <?php
        if (!empty($successMessage)) {
            echo "
        <div class='alert alert-success alert-dismissible fade show' role='alert'>
        <strong>".$successMessage."</strong>
        <button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button>
        ";
        }
        ?>

        <div class="row mb-3">
            <div class="offset-sm-3 col-sm-3 d-grid">
                <button type="submit" class="btn btn-primary">Mettre à jour</button>
            </div>
            <div class="col-sm-3 d-grid">
                <a class="btn btn-outline-primary" href="/" role="button">Annuler</a>
            </div>
        </div>

    </form>
</div>

</body>
</html>
