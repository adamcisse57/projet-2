<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mon Site</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css">
</head>
<body>
<div class="container my-5">
    <h2>Liste des Clients</h2>
    <a class="btn btn-primary" href="./creation.php" role="button">Nouveau client</a>
    <!-- Bouton pour créer un nouveau client -->
    <a class="btn btn-primary" href="./index.php" role="button">Deconnexion</a>
    <br>
    <br>
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nom</th>
            <th>Email</th>
            <th>Téléphone</th>
            <th>Adresse</th>
            <th>Date de Création</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "monsite";

        // création de la connexion
        $conn = new mysqli($servername, $username, $password, $dbname);

        // vérification de la connexion
        if ($conn->connect_error) {
            die("Connexion échouée: " . $conn->connect_error);
        }

        // lire les lignes de la table de la BDD
        $sql = "SELECT * FROM clients";
        $result = $conn->query($sql);

        if (!$result) {
            die("Requête invalide: " . $conn->error);
        }

        // lire les données de chaque ligne
        while ($row = $result->fetch_assoc()) {
            echo "
            <tr>
                <td>$row[id]</td>
                <td>$row[nom]</td>
                <td>$row[email]</td>
                <td>$row[telephone]</td>
                <td>$row[adresse]</td>
                <td>$row[date_de_creation]</td>
                <td>
                    <a class='btn btn-primary btn-sm' href='./modif.php?id=$row[id]'>Modification</a>
                    <a class='btn btn-danger btn-sm' href='supprime.php?id=$row[id]'>Suppression</a>
                </td>
            </tr>
            ";
        }

        // Fermer la connexion
        $conn->close();
        ?>
        </tbody>
    </table>
</div>
</body>
</html>
