<?php
require 'config.php'; // Inclure la connexion à la base de données

$errorMessage = "";
$successMessage = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = $_POST["username"];
    $password = $_POST["password"];
    $hashed_password = password_hash($password, PASSWORD_BCRYPT);

    do {
        if (empty($username) || empty($password)) {
            $errorMessage = "Veuillez remplir tous les champs.";
            break;
        }

        // Insérer un nouvel utilisateur dans la base de données
        $sql = "INSERT INTO utilisateurs (nom_utilisateur, mot_de_passe) VALUES (?, ?)";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("ss", $username, $hashed_password);

        if (!$stmt->execute()) {
            $errorMessage = "Erreur lors de l'enregistrement de l'utilisateur.";
            break;
        }

        $successMessage = "Utilisateur enregistré avec succès.";

        // Redirection après succès
        header("Location: index.php");
        exit;

    } while (false);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Enregistrement</title>
    <link rel="stylesheet" href="styles/style.css"> <!-- Lien vers le CSS -->
</head>
<body>

<div class="container">
    <h2>Enregistrement</h2>

    <?php if (!empty($errorMessage)): ?>
        <div class="alert alert-warning">
            <strong><?php echo $errorMessage; ?></strong>
        </div>
    <?php endif; ?>

    <form method="post">
        <div class="mb-3">
            <label for="username">Nom d'utilisateur</label>
            <input type="text" name="username" id="username" class="form-control" required>
        </div>
        <div class="mb-3">
            <label pour="password">Mot de passe</label>
            <input type="password" name="password" id="password" class="form-control" required>
        </div>
        <button type="submit" class="btn btn-primary">S'enregistrer</button>
    </form>
</div>

</body>
</html>
