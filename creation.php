<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "monsite";

// connection BDD

$connection = new mysqli($servername, $username, $password, $dbname);


$nom = "";
$email = "";
$telephone = "";
$adresse = "";

$errorMessage = "";
$successMessage = "";

if ( $_SERVER["REQUEST_METHOD"] == "POST" ) {
    $nom = $_POST["nom"];
    $email = $_POST["email"];
    $telephone = $_POST["telephone"];
    $adresse = $_POST["adresse"];

    do {
        if (empty($nom) || empty($email) || empty($telephone) || empty($adresse)) {
            $errorMessage = "Veuillez remplir tous les champs";
            break;
        }

        // ajouter des nouveaux clients dans la BDD

        $sql = "INSERT INTO clients (nom, email, telephone, adresse)" .
               " VALUES ('$nom', '$email', '$telephone', '$adresse')";
        $result = $connection->query($sql);

        if (!$result) {
            $errorMessage = "requête invalide: " . $connection->error;
            break;
        }

        $nom = "";
        $email = "";
        $telephone = "";
        $adresse = "";

        $successMessage = "ajout de client avec succes";

       header("Location: Home.php");
       exit;


    } while (false);

}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mon Site</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>

<div class="container my-5">
     <h2>Nouveau Client</h2>

    <?php
    if (!empty($errorMessage)) {
        echo "
        <div class='alert alert-warning alert-dismissible fade show' role='alert'>
        <strong>".$errorMessage."</strong>
        <button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button>
        ";



    }

    ?>


    <form method="post">
        <div class="row mb-3">
            <label class="col-sm-3 col-form-label">Nom</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="nom" value="<?php echo $nom; ?>">

            </div>
        </div>

        <div class="row mb-3">
            <label class="col-sm-3 col-form-label">Email</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="email" value="<?php echo $email; ?>">

            </div>
        </div>

        <div class="row mb-3">
            <label class="col-sm-3 col-form-label">Telephone</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="telephone" value="<?php echo $telephone; ?>">

            </div>
        </div>

        <div class="row mb-3">
            <label class="col-sm-3 col-form-label">Adresse</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="adresse" value="<?php echo $adresse; ?>">

            </div>
        </div>



        <?php
        if ( !empty($successMessage) ) {
            echo "
        <div class='alert alert-warning alert-dismissible fade show' role='alert'>
        <strong>".$successMessage."</strong>
        <button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'></button>
        ";

        }
        ?>

        <div class="row mb-3">
            <div class="offset-sm-3 col-sm-3 d-grid">
                <button type="submit" class="btn btn-primary">Entrer</button>
            </div>
            <div class="col-sm-3 d-grid">
                <a class="btn btn-outline-primary" href="/" role="button">Annuler</a>
            </div>
        </div>

    </form>
</div>

</body>
</html>